# Project Title

Cameo is website and mobile application for social activity like sharing video clips and hire any talents for customized videos.

## Getting Started

Follow below steps to run project

### Prerequisites

Node js, Express, MongooDb

```
Node : node -v - check node version 

```

### Installing

Download and clone repository from here with commant: 

git clone http://gitlab/cameo/test.git

cd project/ -- run this command to go on you project directory

npm install

``````````````````````````````````````
Create a .env file for configuration your project

# Express js port
PORT=8000

# MONGO URI
MONGO_URI=mongodb://127.0.0.1:8000/cameo?authSource=admin

``````
Make sure port is avaliable to run express and MongoDb is runing in your system. Then run below command

nodemon bin\www
node bin\www

```

Hit API 
http://127.0.0.1:8000/test

#Output of above GET API : "Hello , you are doing best. everything is fine go ahead"

Congratulations !! Everyting fine now :) 


### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](https://test.com) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

0.1 - 11-02-2019 created

## Authors

* **Mukesh Kumar** - *Initial work* - [Durapid](https://durapid.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Thanks to express generator
* etc
