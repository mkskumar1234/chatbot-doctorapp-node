let Location = require("../../../models/Location");

class LocationController {
    async add(req, res) {
        const { title } = req.body;
        try {
            const alreadyExist = await Location.findOne({ title }).lean();
            if(alreadyExist){
                return res.status(200).json({ status: false, msg: 'Location already exists.' })
            }
            let location = new Location();
            location.title = title;

            location.save();

            return res.status(200).json({ status: true, msg: 'Location added succesfully.' })
        } catch (error) {
            return res.status(400).json({ status: false, msg: 'Something went wrong.' })
        }
    }

    async view(req, res) {
        const { _id } = req.params;
        try {
            const result = await Location.findOne({ _id });

            return res.status(200).json({ data: result, msg: 'Location details.' })
        } catch (error) {
            console.log("--error", error)
            return res.status(400).json({ msg: "Something went wrong" })
        }
    }

    async update(req, res) {
        const { id, title } = req.body;
        try {
            const checkLocation = await Location.findOne({ _id: id });
            if (!checkLocation) {
                return res.send({status: false,  msg: 'Location not exists with this.' })
            }

            const checkDuplicate = await Location.findOne({ _id: { $ne: id }, title });
            if (checkDuplicate) {
                return res.send({status: false,  msg: 'Location already exists.' })
            }

            checkLocation.title = title;

            checkLocation.save();

            return res.send({status: true,  msg: 'Location updated succesfully.' })
        } catch (error) {
            console.log("--error", error)
            return res.send({status: false,  msg: "Something went wrong" })
        }
    }

    async updateStatus(req, res) {
        const { _id, status } = req.body;
        try {
            const checkLocation = await Location.findOne({ _id });
            if (!checkLocation) {
                return res.send({status: false,  msg: 'Location not exists with this.' })
            }

            checkLocation.status = status;
            checkLocation.save();

            return res.send({status: true,  msg: 'Status updated succesfully.' })
        } catch (error) {
            console.log("--error", error)
            return res.send({status: false,  msg: "Something went wrong" })
        }
    }

    async edit(req, res) {
        const { id } = req.body;
        try {
            const checkLocation = await Location.findOne({ _id: id });
            if (!checkLocation) {
                res.send({status: false,  msg: 'Location not exists with this.' })
            }
            res.send({status: true, data: checkLocation });
        } catch (error) {
            console.log("--error", error)
            res.send({status: false,  msg: 'Something went wrong' })
        }
    }

    async delete(req, res) {
        console.log("00req.params",req.params)
        const { _id } = req.params;
        try {
            const checkLocation = await Location.findOne({ _id });
            if (!checkLocation) {
                return res.send({status: false,  msg: 'Location not exists with this.' })
            }

            checkLocation.is_deleted = true;
            checkLocation.save();

            return res.send({status: true,  msg: 'Location archived successfully.' })
        } catch (error) {
            console.log("--error", error)
            return res.send({status: false,  msg: "Something went wrong" })
        }
    }

    async list(req, res){
        try {
            let data = await Location.find({is_deleted: false}).sort({ _id: -1 });
            
            global.current_tab = "Location List";
            res.render('views/location/list', {categories: data,baseurl: process.env.S3_BASE_URL });
        } catch (error) {
            global.current_tab = "Dashboard";
            res.render('views/dashboard');
        }
    }
}

module.exports = new LocationController();