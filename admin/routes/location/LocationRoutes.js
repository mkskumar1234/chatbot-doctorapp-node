const express = require("express"),
    router = express.Router(),
    LocationController = require("./LocationController"),
    AuthMainAdmin = require('../../../_helpers/authMainAdmin');


router.get("/", AuthMainAdmin, LocationController.list);

router.post("/add",AuthMainAdmin, LocationController.add);

router.post("/edit",AuthMainAdmin, LocationController.edit);

router.delete("/:_id",AuthMainAdmin, LocationController.delete);

router.post("/update",AuthMainAdmin, LocationController.update);

router.post("/update-status",AuthMainAdmin, LocationController.updateStatus);

module.exports = router;