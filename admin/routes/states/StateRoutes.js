const express = require("express"),
    router = express.Router(),
    DepartmentController = require("./StateController"),
    AuthMainAdmin = require('../../../_helpers/authMainAdmin');


router.get("/", AuthMainAdmin, DepartmentController.list);

router.all("/add",AuthMainAdmin, DepartmentController.add);

// router.post("/update-status",AuthMainAdmin, DepartmentController.updateStatus);

module.exports = router;