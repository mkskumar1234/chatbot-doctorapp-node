let State = require("../../../models/State");

class StateController {
    async add(req, res) {
        if (req.method == 'POST') {
            const { title } = req.body;
            try {
                const alreadyExist = await State.findOne({ title }).lean();
                if (alreadyExist) {
                    return res.status(200).json({ status: false, msg: 'State already exists.' })
                }
                let states = new State();
                states.title = title;

                states.save();

                return res.status(200).json({ status: true, msg: 'State added succesfully.' })
            } catch (error) {
                return res.status(400).json({ status: false, msg: 'Something went wrong.' })
            }
        } else {
            global.current_tab = "State Add";
            res.render('views/states/add');
        }

    }

    async view(req, res) {
        const { _id } = req.params;
        try {
            const result = await State.findOne({ _id });

            global.current_tab = "State Add";
            res.render('views/states/view', {data: result});
        } catch (error) {
            console.log("--error", error)
            return res.status(400).json({ msg: "Something went wrong" })
        }
        }

    async update(req, res) {
        const { id, title } = req.body;
        try {
            const checkState = await State.findOne({ _id: id });
            if (!checkState) {
                return res.send({ status: false, msg: 'State not exists with this.' })
            }

            const checkDuplicate = await State.findOne({ _id: { $ne: id }, title });
            if (checkDuplicate) {
                return res.send({ status: false, msg: 'State already exists.' })
            }

            checkState.title = title;

            checkState.save();

            return res.send({ status: true, msg: 'State updated succesfully.' })
        } catch (error) {
            console.log("--error", error)
            return res.send({ status: false, msg: "Something went wrong" })
        }
    }

    async updateStatus(req, res) {
        const { _id, status } = req.body;
        try {
            const checkState = await State.findOne({ _id });
            if (!checkState) {
                return res.send({ status: false, msg: 'State not exists with this.' })
            }

            checkState.status = status;
            checkState.save();

            return res.send({ status: true, msg: 'Status updated succesfully.' })
        } catch (error) {
            console.log("--error", error)
            return res.send({ status: false, msg: "Something went wrong" })
        }
    }

    async edit(req, res) {
        const { id } = req.body;
        try {
            const checkState = await State.findOne({ _id: id });
            if (!checkState) {
                res.send({ status: false, msg: 'State not exists with this.' })
            }
            res.send({ status: true, data: checkState });
        } catch (error) {
            console.log("--error", error)
            res.send({ status: false, msg: 'Something went wrong' })
        }
    }

    async delete(req, res) {
        console.log("00req.params", req.params)
        const { _id } = req.params;
        try {
            const checkState = await State.findOne({ _id });
            if (!checkState) {
                return res.send({ status: false, msg: 'State not exists with this.' })
            }

            checkState.is_deleted = true;
            checkState.save();

            return res.send({ status: true, msg: 'State archived successfully.' })
        } catch (error) {
            console.log("--error", error)
            return res.send({ status: false, msg: "Something went wrong" })
        }
    }

    async list(req, res) {
        try {
            let data = await State.find({ is_deleted: false }).sort({ _id: -1 });

            global.current_tab = "State List";
            res.render('views/states/list', { data });
        } catch (error) {
            global.current_tab = "Dashboard";
            res.render('views/dashboard');
        }
    }
}

module.exports = new StateController();