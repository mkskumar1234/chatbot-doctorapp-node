const express = require("express"),
    router = express.Router(),
    UserController = require("./UserController"),
    AuthMainAdmin = require('../../../_helpers/authMainAdmin');


router.get("/", AuthMainAdmin, UserController.list);

router.get("/:_id", AuthMainAdmin, UserController.view);

router.post("/update-status", AuthMainAdmin, UserController.updateStatus);





module.exports = router;