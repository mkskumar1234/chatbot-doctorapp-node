let User = require("../../../models/User");

class UserController {
    async list(req, res) {
        try {
            let result = await User.find({ is_deleted: false, user_type: 'U' }).sort({ _id: -1 });

            global.current_tab = "User List";
            res.render('views/users/list', { data: result, baseurl: process.env.S3_BASE_URL });
        } catch (error) {
            global.current_tab = "Dashboard";
            res.render('views/dashboard');
        }
    }

    async view(req, res) {
        const { _id } = req.params;
        try {
            let result = await User.findOne({ _id, is_deleted: false, user_type: 'U', }).lean();
            console.log("--result", result)
            global.current_tab = "User View";
            res.render('views/users/view', { data: result, baseurl: process.env.S3_BASE_URL });
        } catch (error) {
            global.current_tab = "Dashboard";
            res.render('views/dashboard');
        }
    }

    async updateStatus(req, res) {
        const { _id, status } = req.body;
        try {
            const checkResult = await User.findOne({ _id, user_type: 'U' });
            if (!checkResult) {
                return res.send({status: false,  msg: 'User not exists.' })
            }

            checkResult.status = status;
            checkResult.save();

            return res.send({status: true,  msg: 'Status updated succesfully.' })
        } catch (error) {
            console.log("--error", error)
            return res.send({status: false,  msg: "Something went wrong" })
        }
    }

    


}

module.exports = new UserController();