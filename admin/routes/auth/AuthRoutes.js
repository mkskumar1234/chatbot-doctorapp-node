const express = require("express"),
    router  = express.Router(),
    AuthController = require("./AuthController"),
    AuthMainAdmin = require('../../../_helpers/authMainAdmin');

    
router.get("/", AuthMainAdmin, function(req, res, next){
    global.current_tab = "Login";
    res.render('views/login');
});

router.get("/login", function(req, res, next){
    global.current_tab = "Login";
    res.render('views/login');
});

router.post("/login", AuthController.login);

router.get("/dashboard", AuthMainAdmin, async (req, res, next) => {
    global.current_tab = "Dashboard";
    res.render('views/dashboard');
});

router.get('/logout', function(req, res, next) {
    try {
      res.cookie('auth', '');
      res.redirect('/login');
    } 
    catch (error) {
      res.next(error);
    }
});

router.get('/forgot-password', function (req, res, next) {
    global.current_tab = "Forget Password";
    res.render('views/forgot_password',{
      error: ''
    });
});

router.post("/forgot-password", AuthController.forgotPassword);

router.all("/change-password",  AuthMainAdmin, AuthController.changePassword);


module.exports = router;