const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { ObjectId } = require('mongodb');

let Admin = require("../../../models/Admin");
let EmailTemplate = require("../../../models/EmailTemplate")

class AuthController {
    async login(req, res) {
        const { email } = req.body;
        console.log("--req.body", req.body)
        try {
            let checkUser = await Admin.findOne({ email }).lean();
            if (!checkUser) {
                res.send({ status: false, msg: 'User not exists with this email id' });
            }
            // check user password
            if (!bcrypt.compareSync(req.body.password, checkUser.password)) {
                res.send({ status: false, msg: 'Invalid login details.' });
            }
            // login token set here
            const token = jwt.sign({ sub: checkUser._id }, process.env.SECRET_KEY)

            res.cookie('auth', token);
            res.send({ status: true, token: token, msg: 'LogedIn successfully.' });

        } catch (error) {
            console.log("--error", error)
            res.send({ status: false, msg: "Something went wrong" });

        }
    }

    async forgotPassword(req, res) {
        const { email } = req.body;
        try {
            let dataObj = await EmailTemplate.findOne({ template_key: 'forgotPassword' });
            var emailContent = dataObj.content;
            var forgot_pass_ver_code = FUNC.randonString();
            var datetime = new Date();
            datetime.setDate(datetime.getDate() + 2);
            datetime = datetime.getTime() / 1000;
            var verification_expire = datetime;

            Admin.findOneAndUpdate(
                { email: new RegExp(email, 'i') },
                { $set: { forgot_pass_ver_code, verification_expire } },
                function (err, updatedObj) {
                    if (err) {
                        res.send({ status: false, msg: "Something went wrong" });
                    }
                    else {
                        if (updatedObj == '' || updatedObj === null) {
                            res.send({ status: false, msg: "Email address not exists with our system." });
                        }else {
                            var hrefLink = '<a href="' + process.env.ADMIN_URL + 'reset-password/' + forgot_pass_ver_code + '"> Reset Password</a>';
                            var logoimg = '<img src="' + process.env.LOGO_LINK + '" >';
                            var mapObj = {
                                LOGO_LINK: logoimg,
                                USER_NAME: updatedObj.username,
                                LINK_URL: hrefLink
                            };
                            emailContent = emailContent.replace(/LOGO_LINK|USER_NAME|LINK_URL/gi, function (matched) {
                                return mapObj[matched];
                            });
                            var mailData = {
                                from: process.env.FROM_MAIL,
                                to: updatedObj.email,
                                subject: dataObj.subject,
                                html: emailContent
                            };
                            FUNC.sendMail(mailData, function (cb) {
                                if (cb) {
                                    res.send({ status: false, msg: "Email cant sent yet. Please try again or contact to service provider." });
                                } else {
                                    res.send({ status: true, msg: "A varification email sent to your registered email. Please reset your password." });
                                }
                            });
                        }
                    }
                });
        } catch (error) {
            console.log("--error",error)
            res.send({ status: false, msg: "Something went wrong" });
        }           
    }

    async changePassword(req, res){
        if(req.method=='POST'){
            console.log("--req.body",req.body)
            const { current_password, password, password_confirmation } = req.body;
            try {
                let self = await Admin.findOne({email : "admin@admin.com"});
               
                if (!bcrypt.compareSync(current_password, self.password)) {
                    return res.send({ status: false, msg: 'Current password not matched.' });
                }
                if (password !== password_confirmation) {
                    return res.send({ status: false, msg: 'New password and confirm password not matched.' });
                }
                return res.send({ status: true, msg: "Password changed successfully." });

            }catch(error){
                console.log("-error",error)
                return res.send({ status: false, msg: "Something went wrong" });
            }
        }else{
            global.current_tab = "Change Password";
            res.render('views/users/change-password');
        }
    }
}

module.exports = new AuthController();