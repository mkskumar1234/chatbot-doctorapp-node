var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();
global.site_title = process.env.SITE_TITLE;


//model
var Admin = require('../../models/Admin');
// const Country = require("../../models/Country");
// const User = require("../../models/User");
// const Category = require("../../models/Category");
// const EmailTemplate = require('../../models/EmailTemplate');
// var Settings = require('../../models/Setting');

//auth middlewares
var AuthMainAdmin = require('../../_helpers/authMainAdmin');

/* open maintenance page
* URl: /admin/maintenance
* Required Param :
* Optional Param :
* Return : view
*/
// router.get('/maintenance', AuthMainAdmin, async function (req, res, next) {
//   if (req.adminroleis == 'mainadmin') {
//     res.redirect('/admin/dashboard');
//   } 
//   else if (req.adminroleis == 'subadmin') {
    
//     settings = await Settings.findOne({});
//     if (settings.maintenance) {
//       global.current_tab = "Maintenance";
//       res.render('views/maintainence', { settings: settings, adminroleis: req.adminroleis });
//     }
//     else{
//       res.redirect('/admin/dashboard');
//     }
    
//   }
//   else{
//     res.redirect('/admin/login');
//   }
// });

/* open login page
* URl: /admin/login
* Required Param :
* Optional Param :
* Return : view
*/
router.get('/', AuthMainAdmin, function (req, res, next) {
  console.log("--test")
  // if (req.adminroleis == 'mainadmin' && req.adminroleis == 'subadmin') {
  //   res.redirect('/admin/dashboard');
  // } else {
  global.current_tab = "Login";
  res.render('views/login');
  // }
});

/* open login page
* URl: /admin/login
* Required Param :
* Optional Param :
* Return : view
*/
router.get('/login', function (req, res, next) {
  if (req.adminroleis == 'mainadmin' && req.adminroleis == 'subadmin') {
    res.redirect('/admin/dashboard');
  } else {
  global.current_tab = "Login";
  res.render('views/login');
  }
});

/* open forgot password page
* URl: /admin/forgot-password
* Required Param :
* Optional Param :
* Return : view
*/
router.get('/forgot-password', function (req, res, next) {
  if (req.adminroleis == 'mainadmin' && req.adminroleis == 'subadmin') {
    res.redirect('/admin/dashboard');
  } else {
  global.current_tab = "Forget Password";
  res.render('views/forgot_password',{
    error: ''
  });
  }
});


/* open reset password page
* URl: /admin/reset-password/token
* Required Param :reset_token
* Optional Param :
* Return : view
*/
router.get('/reset-password/:token', async function (req, res, next) {
  var admins = await Admin.findOne({ forgot_pass_ver_code: req.params.token });
  if (admins === null) {
    global.current_tab = "Forget Password";
    res.render('views/forgot_password', {
      error: 'Reset Password Request Not Found, Please Request a New Password Reset Email.'
    });
  }else{
    global.current_tab = "Reset Password";
    res.render('views/reset_pass', {
      tokenis: req.params.token
    });
  }
  
});

/* open dashboard page
* URl: /admin/dashboard
* Required Param :
* Optional Param :
* Return : view
*/
router.get('/dashboard', AuthMainAdmin , async function(req, res, next) {
  if (req.adminroleis == 'mainadmin') {
  global.current_tab = "Dashboard";
    var allnormalusers = await User.count({ role: 'normal', $or: [{ isDeleted: /false/ }, { isDeleted: null }] }).sort({ _id: -1 });
    var allshopowner = await User.count({ role: 'shopowner', $or: [{ isDeleted: /false/ }, { isDeleted: null }] }).sort({ _id: -1 });
    var allveterinarian = await User.count({ role: 'veterinarian', $or: [{ isDeleted: /false/ }, { isDeleted: null }] }).sort({ _id: -1 });
    var allcategories = await Category.count({ $or: [{ isDeleted: /false/ }, { isDeleted: null }] }).sort({ _id: -1 });
    res.render('views/dashboard', { normalusers: allnormalusers, shopowner: allshopowner, veterinarian: allveterinarian, categories: allcategories, adminroleis : req.adminroleis});
  }
  else {
    global.current_tab = "Dashboard";
    var allnormalusers = await User.count({ role: 'normal', 'address.country': new RegExp(["^", req.decoded.logindata.country, "$"].join(""), "i"), $or: [{ isDeleted: /false/ }, { isDeleted: null }] }).sort({ _id: -1 });
    var allshopowner = await User.count({ role: 'shopowner', 'address.country': new RegExp(["^", req.decoded.logindata.country, "$"].join(""), "i"), $or: [{ isDeleted: /false/ }, { isDeleted: null }] }).sort({ _id: -1 });
    var allveterinarian = await User.count({ role: 'veterinarian', 'address.country': new RegExp(["^", req.decoded.logindata.country, "$"].join(""), "i"), $or: [{ isDeleted: /false/ }, { isDeleted: null }] }).sort({ _id: -1 });
    res.render('views/dashboard', { normalusers: allnormalusers, shopowner: allshopowner, veterinarian: allveterinarian, adminroleis: req.adminroleis });
  }
}); 

/* open subadmin listing page
* URl: /admin/subadmin
* Required Param :
* Optional Param :
* Return : view
*/
router.get('/subadmin', AuthMainAdmin, async function (req, res, next) {
  if (req.adminroleis == 'mainadmin') {
  global.current_tab = "Edit Sub-Admin";
  var subadmins = await Admin.find({ role: 'subadmin', $or: [{ isDeleted: /false/ }, { isDeleted: null }]}).sort({ _id: -1 });
  res.render('views/subadminlisting', { subadmins: subadmins, adminroleis: req.adminroleis });
  }
  else {
    res.redirect('/404');
  }
}); 

/* open add subadmin page
* URl: /admin/subadmin/add
* Required Param :
* Optional Param :
* Return : view
*/
router.get('/subadmin/add', AuthMainAdmin, async function (req, res, next) {
  if (req.adminroleis == 'mainadmin') {
  global.current_tab = "Add Sub-Admin";
  var countries = await Country.find({ status: true }).sort({ code: 1 });
  res.render('views/addsubadmin', { countries: countries, adminroleis: req.adminroleis});
  }
  else {
    res.redirect('/404');
  }
}); 

/* register sub-admin
* URl: /admin/subadmin/add
* Required Param :username, email, password,country
* Optional Param :mobile
* Return : Obj
*/
router.post('/subadmin/add', AuthMainAdmin, function (req, res, next) { 
  if (req.adminroleis == 'mainadmin') {
  try {
    Admin.count({ $or: [{ username: new RegExp(["^", req.body.username, "$"].join(""), "i") }, { email: new RegExp(["^", req.body.email, "$"].join(""), "i") }  ], isDeleted:'false' }).exec(async function (err, datacount) {
    if (datacount == 0) {
      var admin = new Admin();
      admin.username = req.body.username;
      admin.email = req.body.email;
      admin.password = bcrypt.hashSync(req.body.password, 10);
      admin.country = req.body.country;
      admin.mobile = req.body.mobile;
      await admin.save();
      res.send({
        status: 'success'
      });
    }
    else{
      res.send({
        status: 'exists'
      });
    }
    
    });
  }
  catch (error) {
    next(error);
  }
  }
  else {
    res.send({
      status: 'not authorized'
    });
  }
});

/* update subadmin activation status
* URl: /admin/subadmin/updateactivation
* Required Param :uid,status_code
* Optional Param :
* Return : Obj
*/
router.post('/subadmin/updateactivation', AuthMainAdmin, function (req, res, next) {
  if (req.adminroleis == 'mainadmin') {
  try {
    Admin.findOne({ _id: req.body.uid }, function (error, data) {
      if (error) {
        res.send({
          status: 'error'
        });
      }
      else {
        if (req.body.status_code != '') {
          if (req.body.status_code == 'true') {
            var statusis = true;
          }
          if (req.body.status_code == 'false') {
            var statusis = false;
          }
          if (data != null) {
            data.status = statusis;
            data.save();
            res.send({
              status: 'success'
            });
          } else {
            res.send({
              status: 'not exists'
            });
          }
        }
        else {
          res.send({
            status: 'error'
          });
        }
      }
    });
  }
  catch (error) {
    res.send({
      status: 'error'
    });
  }
  }
    else {
    res.send({
      status: 'not authorized'
    });
  }
});

/* open edit subadmin page
* URl: /admin/subadmin/:id
* Required Param :id
* Optional Param :
* Return : view
*/
router.get('/subadmin/:id', AuthMainAdmin, async function (req, res, next) {
  if (req.adminroleis == 'mainadmin') {
  global.current_tab = "Edit Sub-Admin";
  var countries = await Country.find({ status: true }).sort({ code: 1 });
  var subadmin = await Admin.findOne({ _id: req.params.id });
  res.render('views/editsubadmin', { countries: countries, subadmin: subadmin, adminroleis: req.adminroleis });
  }
  else {
    res.redirect('/404');
  }
}); 

/* update sub-admin
* URl: /admin/subadmin/update/:id
* Required Param :id,username, email,country
* Optional Param :mobile
* Return : Obj
*/
router.put('/subadmin/update/:id', AuthMainAdmin, function (req, res, next) {
  if (req.adminroleis == 'mainadmin') {
    try {
      Admin.count({ $or: [{ username: new RegExp(["^", req.body.username, "$"].join(""), "i") }, { email: new RegExp(["^", req.body.email, "$"].join(""), "i") }], isDeleted: 'false', _id: { $ne: req.params.id } }).exec(async function (err, datacount) {
        if (datacount == 0) {
          var admin = await Admin.findOne({_id:req.params.id});
          admin.username = req.body.username;
          admin.email = req.body.email;
          admin.country = req.body.country;
          admin.mobile = req.body.mobile;
          await admin.save();
          res.send({
            status: 'success'
          });
        }
        else if (datacount >= 1) {
          res.send({
            status: 'exists'
          });
        }
        else {
          res.send({
            status: 'not exists'
          });
        }

      });
    }
    catch (error) {
      next(error);
    }
  }
    else {
    res.send({
      status: 'not authorized'
    });
  }
});

/* soft delete a subadmin
* URl: /admin/subadmin/delete/:id
* Required Param :id
* Optional Param :
* Return : Obj
*/
router.delete('/subadmin/delete/:id', AuthMainAdmin, function (req, res, next) {
  if (req.adminroleis == 'mainadmin') {
  try {
    Admin.findOne({ _id: req.params.id }, function (error, data) {
      if (error) {
        res.send({
          status: 'error'
        });
      }
      else {
        if (data != null) {
          data.isDeleted = 'true';
          data.save();
          res.send({
            status: 'success'
          });
        }
        else {
          res.send({
            status: 'not exists'
          });
        }
      }
    });
  }
  catch (error) {
    res.send({
      status: 'error'
    });
  }
  }
  else {
    res.send({
      status: 'not authorized'
    });
  }
});

/* login admin
* URl: /admin/login
* Required Param :username, password
* Optional Param :
* Return : Obj
*/
router.post('/login', async function (req, res, next) {
  try {
    var userdetail = await Admin.findOne({ $or: [{ username: new RegExp(req.body.username, 'i') }, { email: new RegExp(req.body.username, 'i') }] });
    if (userdetail != null) {
      if (bcrypt.compareSync(req.body.password, userdetail.password) && userdetail.isDeleted == 'false' && userdetail.status === true) {
          const JWTToken = jwt.sign({
          email: userdetail.email,
          _id: userdetail._id,
          logindata:userdetail,
          role:userdetail.role
        },
        process.env.SESSION_SECRET,
        {
          expiresIn: 86400
        });
        res.cookie('auth', JWTToken);
        res.send({ status: 'login success', token: JWTToken, role: userdetail.role});
      } 
      else {
        res.send({ status: 'invalid password' });
      }
    }
    else {
      res.send({ status: 'not exists' });
    }
  }
  catch (error) {
    res.next(error);
  }
}); 

/* logout admin
* URl: /admin/logout
* Required Param :
* Optional Param :
* Return : Obj
*/
router.get('/logout', function(req, res, next) {
  try {
    res.cookie('auth', '');
    res.redirect('/login');
  } 
  catch (error) {
    res.next(error);
  }
});

/* send password reset email for admin/sub-admin
* URl: /admin/forgot_pass
* Required Param :username
* Optional Param :
* Return : Obj
*/
router.post('/forgot_pass', async function (req, res, next) {

      await EmailTemplate.findOne({ template_key: 'forgotPassword' }, function (err, dataObj) {
        if (err) {
          res.send({ status: 'error' });
        }
        else{
          var emailContent = dataObj.content;
          var forgot_pass_ver_code = FUNC.randonString();
          var datetime = new Date();
          datetime.setDate(datetime.getDate() + 2);
          datetime = datetime.getTime() / 1000;
          var verification_expire = datetime;


          Admin.findOneAndUpdate(
            { $or: [{ username: new RegExp(req.body.username, 'i') }, { email: new RegExp(req.body.username, 'i') }] },
            { $set: { forgot_pass_ver_code, verification_expire } },
            function (err, updatedObj) {
              if (err) {
                res.send({ status: 'error' });
              }
              else{

                if (updatedObj == '' || updatedObj === null) {
                  res.send({ status: 'not exists' });
                }
                else{
                
                var hrefLink = '<a href="'+process.env.ADMIN_URL + 'admin/reset-password/' + forgot_pass_ver_code + '"> Reset Password</a>';
                  var logoimg = '<img src="' + process.env.LOGO_LINK+'" >';
                var mapObj = {
                  LOGO_LINK: logoimg,
                  USER_NAME: updatedObj.username,
                  LINK_URL: hrefLink
                };
                  emailContent = emailContent.replace(/LOGO_LINK|USER_NAME|LINK_URL/gi, function (matched) {
                  return mapObj[matched];
                });
                var mailData = {
                  from: process.env.FROM_MAIL,
                  to: updatedObj.email,
                  subject: dataObj.subject,
                  html: emailContent
                };
                FUNC.sendMail(mailData, function (cb) {
                  if (cb) {
                    res.send({ status: 'error' });
                  } else {
                    res.send({ status: 'success' });
                  }
                });
              }
              }
            });
        }
      });
});

/* open countries listing page
* URl: /admin/countries
* Required Param :
* Optional Param :
* Return : view
*/
router.get('/countries', AuthMainAdmin , async function (req, res, next) {
  global.current_tab = "Countries";
  var countries = await Country.find().sort({ code: 1 });
  res.render('views/countries', { countries: countries, adminroleis: req.adminroleis });
});

/* update country activation status
* URl: /admin/countries/updateactivation
* Required Param :cid,status_code
* Optional Param :
* Return : Obj
*/
router.post('/countries/updateactivation', AuthMainAdmin, function (req, res, next) {
  if (req.adminroleis == 'mainadmin') {
    try {
      Country.findOne({ _id: req.body.cid }, function (error, data) {
        if (error) {
          res.send({
            status: 'error'
          });
        }
        else {
          if (req.body.status_code != '') {
            if (req.body.status_code == 'true') {
              var statusis = true;
            }
            if (req.body.status_code == 'false') {
              var statusis = false;
            }
            if (data != null) {
              data.status = statusis;
              data.save();
              res.send({
                status: 'success'
              });
            } else {
              res.send({
                status: 'not exists'
              });
            }
          }
          else {
            res.send({
              status: 'error'
            });
          }
        }
      });
    }
    catch (error) {
      res.send({
        status: 'error'
      });
    }
  }
  else {
    res.send({
      status: 'not authorized'
    });
  }
});

router.post('/reset-password/:token', async function (req, res, next) {
  var admins = await Admin.findOne({ forgot_pass_ver_code: req.params.token });
  if (admins === null) {
    global.current_tab = "Forget Password";
    res.render('views/forgot_password', {
      error: 'Reset Password Request Not Found, Please Request a New Password Reset Email.'
    });
  } else {
    var today = new Date();
    var expires_at = new Date(admins.verification_expire * 1000);

    if (today > expires_at) {
      res.send({
        status: 'expired',
        msg:'This Password Reset Request is Expired, Please Request a New Password Reset Email.'
      });
    }
    else{

      var new_password = bcrypt.hashSync(req.body.password, 10)

      Admin.findOneAndUpdate({
        _id: admins._id
      },{
        $set:{
          password: new_password
        },
        $unset:{
          forgot_pass_ver_code: true,
          verification_expire: true,
        }
        }, function (err, userInfo) {
          if(err){
            res.send({
              status: 'error'
            });
          }
          else{
            res.send({
              status: 'success'
            });
            }
      })
    }
    
  }
});

module.exports = router;
