let EmailTemplate = require("../../../models/EmailTemplate");

class EmailTemplateController{
    async list(req, res){
        try {
            let result = await EmailTemplate.find({}).sort({ _id: -1 }).lean();
            global.current_tab = "Email Template List";
            res.render('views/email_templates/list', {data: result });
        } catch (error) {
            global.current_tab = "Dashboard";
            res.render('views/dashboard');
        }
    }

    async edit(req, res){
        if(req.method=='POST'){
            const { _id } = req.params;
            const { subject, content } = req.body;
            console.log("--req.body",req.body)
            try {
                const checkData = await EmailTemplate.findOne({ _id });
                if (!checkData) {
                    return res.send({status: false,  msg: 'Record not exists with this.' })
                }

                checkData.subject = subject;
                checkData.content = content;

                checkData.save();

                return res.send({status: true,  msg: 'Record updated succesfully.' })
            } catch (error) {
                console.log("--error", error)
                return res.send({status: false,  msg: "Something went wrong" })
            }
        }else{
            const { _id } = req.params;
            try {
                const checkData = await EmailTemplate.findOne({ _id });
                if (!checkData) {
                    res.send({status: false,  msg: 'Record not exists.' })
                }
                global.current_tab = "Email Template";
                res.render('views/email_templates/edit', { data: checkData});
            } catch (error) {
                res.send({status: false,  msg: 'Something went wrong' })
            }
            
        }
    }

}

module.exports = new EmailTemplateController();