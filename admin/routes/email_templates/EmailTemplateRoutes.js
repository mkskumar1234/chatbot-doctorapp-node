let express = require("express"),
    router = express.Router(),
    EmailTemplateController = require("./EmailTemplateController"),
    AuthMainAdmin = require('../../../_helpers/authMainAdmin');

router.get("/", AuthMainAdmin, EmailTemplateController.list);

router.all("/:_id", AuthMainAdmin, EmailTemplateController.edit);

module.exports = router;