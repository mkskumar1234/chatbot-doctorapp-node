const express = require("express"),
    router = express.Router(),
    DoctorController = require("./DoctorController"),
    AuthMainAdmin = require('../../../_helpers/authMainAdmin');


router.get("/", AuthMainAdmin, DoctorController.list);

router.all("/add", AuthMainAdmin, DoctorController.add);

router.get("/:_id", AuthMainAdmin, DoctorController.view);

router.post("/update-status", AuthMainAdmin, DoctorController.updateStatus);

module.exports = router;