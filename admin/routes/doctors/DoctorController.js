const Doctor = require("../../../models/Doctor");
const Department = require("../../../models/Department");
const State = require("../../../models/State");


class DoctorController {
    async list(req, res) {
        try {
            let result = await Doctor.find({ is_deleted: false }).populate("department_id",'title').sort({ _id: -1 });

            global.current_tab = "Doctor List";
            res.render('views/doctors/list', { data: result, baseurl: process.env.S3_BASE_URL });
        } catch (error) {
            global.current_tab = "Dashboard";
            res.render('views/dashboard');
        }
    }

    async view(req, res) {
        const { _id } = req.params;
        try {
            let result = await Doctor.findOne({ _id })
                .populate("department_id","title")
                .populate("state_id","title")
                .populate("city_id","title")
                .lean();
            console.log("--result", result)
            global.current_tab = "Doctor View";
            res.render('views/doctors/view', { data: result, baseurl: process.env.S3_BASE_URL });
        } catch (error) {
            global.current_tab = "Dashboard";
            res.render('views/dashboard');
        }
    }

    async updateStatus(req, res) {
        const { _id, status } = req.body;
        try {
            const checkResult = await Doctor.findOne({ _id });
            if (!checkResult) {
                return res.send({ status: false, msg: 'Doctors not exists.' })
            }

            checkResult.status = status;
            checkResult.save();

            return res.send({ status: true, msg: 'Status updated succesfully.' })
        } catch (error) {
            console.log("--error", error)
            return res.send({ status: false, msg: "Something went wrong" })
        }
    }

    async add(req, res) {
        if (req.method == 'POST') {
            const { name, email, department_id, state_id, city_id } = req.body;
            try {
                const alreadyExist = await Doctor.findOne({ email }).lean();
                if (alreadyExist) {
                    return res.status(200).json({ status: false, msg: 'Doctor email already exists.' })
                }
                let doctor = new Doctor();
                doctor.name = name;
                doctor.email = email;
                doctor.department_id = department_id;
                doctor.state_id = state_id;
                doctor.city_id = city_id;

                doctor.save();

                return res.status(200).json({ status: true, msg: 'Doctor added succesfully.' })
            } catch (error) {
                console.log("-error",error)
                return res.status(400).json({ status: false, msg: 'Something went wrong.' })
            }
        } else {
            global.current_tab = "Doctor Add";
            // Get state
            // Get department
            const departments = await Department.find({ is_deleted: false }).sort({ _id: 1 }).lean();
            const states = await State.find({ is_deleted: false }).sort({ _id: 1 }).lean();

            res.render('views/doctors/add', { departments, states });
        }

    }


}

module.exports = new DoctorController();