let express = require("express"),
    router = express.Router(),
    PageController = require("./PageController"),
    AuthMainAdmin = require('../../../_helpers/authMainAdmin');


router.get("/", AuthMainAdmin, PageController.list);

router.all("/add", AuthMainAdmin, PageController.add);

router.all("/:_id", AuthMainAdmin, PageController.edit);

module.exports = router;