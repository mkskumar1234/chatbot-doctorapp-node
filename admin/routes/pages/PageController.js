let Page = require("../../../models/Page");

class PageController{
    async list(req, res){
        try {
            let result = await Page.find({}).sort({ _id: -1 }).lean();
            global.current_tab = "Page List";
            res.render('views/pages/list', {data: result });
        } catch (error) {
            global.current_tab = "Dashboard";
            res.render('views/dashboard');
        }
    }

    async add(req, res){
        if(req.method=='POST'){
            const { title, content } = req.body;
            try {
                let page = new Page();
                page.title = title;
                page.content = content;
                page.slug = "Test";

                page.save();

                return res.send({status: true,  msg: 'Record updated succesfully.' })
            } catch (error) {
                console.log("--error", error)
                return res.send({status: false,  msg: "Something went wrong" })
            }
        }else{
            global.current_tab = "Page Add";
            res.render('views/pages/add');
        }
    }

    async edit(req, res){
        if(req.method=='POST'){
            const { _id } = req.params;
            const { title, content } = req.body;
            try {
                const checkData = await Page.findOne({ _id });
                if (!checkData) {
                    return res.send({status: false,  msg: 'Record not exists with this.' })
                }

                checkData.title = title;
                checkData.content = content;

                checkData.save();

                return res.send({status: true,  msg: 'Record updated succesfully.' })
            } catch (error) {
                return res.send({status: false,  msg: "Something went wrong" })
            }
        }else{
            const { _id } = req.params;
            try {
                const checkData = await Page.findOne({ _id });
                if (!checkData) {
                    res.send({status: false,  msg: 'Record not exists.' })
                }
                global.current_tab = "Page";
                res.render('views/pages/edit', { data: checkData});
            } catch (error) {
                res.send({status: false,  msg: 'Something went wrong' })
            }
            
        }
    }

}

module.exports = new PageController();