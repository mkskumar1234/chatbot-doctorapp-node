const express = require("express"),
    router = express.Router(),
    DepartmentController = require("./DepartmentController"),
    AuthMainAdmin = require('../../../_helpers/authMainAdmin');


router.get("/", AuthMainAdmin, DepartmentController.list);

router.all("/add",AuthMainAdmin, DepartmentController.add);

// router.post("/edit",AuthMainAdmin, DepartmentController.edit);

// router.get("/:_id",AuthMainAdmin, DepartmentController.view);

// router.delete("/:_id",AuthMainAdmin, DepartmentController.delete);

// router.post("/update",AuthMainAdmin, DepartmentController.update);

// router.post("/update-status",AuthMainAdmin, DepartmentController.updateStatus);

module.exports = router;