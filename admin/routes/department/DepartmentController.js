let Department = require("../../../models/Department");

class DepartmentController {
    async add(req, res) {
        if (req.method == 'POST') {
            const { title } = req.body;
            try {
                const alreadyExist = await Department.findOne({ title }).lean();
                if (alreadyExist) {
                    return res.status(200).json({ status: false, msg: 'Department already exists.' })
                }
                let department = new Department();
                department.title = title;

                department.save();

                return res.status(200).json({ status: true, msg: 'Department added succesfully.' })
            } catch (error) {
                return res.status(400).json({ status: false, msg: 'Something went wrong.' })
            }
        } else {
            global.current_tab = "Department Add";
            res.render('views/department/add');
        }

    }

    async view(req, res) {
        const { _id } = req.params;
        try {
            const result = await Department.findOne({ _id });

            global.current_tab = "Department Add";
            res.render('views/department/view', {data: result});
        } catch (error) {
            console.log("--error", error)
            return res.status(400).json({ msg: "Something went wrong" })
        }
        }

    async update(req, res) {
        const { id, title } = req.body;
        try {
            const checkDepartment = await Department.findOne({ _id: id });
            if (!checkDepartment) {
                return res.send({ status: false, msg: 'Department not exists with this.' })
            }

            const checkDuplicate = await Department.findOne({ _id: { $ne: id }, title });
            if (checkDuplicate) {
                return res.send({ status: false, msg: 'Department already exists.' })
            }

            checkDepartment.title = title;

            checkDepartment.save();

            return res.send({ status: true, msg: 'Department updated succesfully.' })
        } catch (error) {
            console.log("--error", error)
            return res.send({ status: false, msg: "Something went wrong" })
        }
    }

    async updateStatus(req, res) {
        const { _id, status } = req.body;
        try {
            const checkDepartment = await Department.findOne({ _id });
            if (!checkDepartment) {
                return res.send({ status: false, msg: 'Department not exists with this.' })
            }

            checkDepartment.status = status;
            checkDepartment.save();

            return res.send({ status: true, msg: 'Status updated succesfully.' })
        } catch (error) {
            console.log("--error", error)
            return res.send({ status: false, msg: "Something went wrong" })
        }
    }

    async edit(req, res) {
        const { id } = req.body;
        try {
            const checkDepartment = await Department.findOne({ _id: id });
            if (!checkDepartment) {
                res.send({ status: false, msg: 'Department not exists with this.' })
            }
            res.send({ status: true, data: checkDepartment });
        } catch (error) {
            console.log("--error", error)
            res.send({ status: false, msg: 'Something went wrong' })
        }
    }

    async delete(req, res) {
        console.log("00req.params", req.params)
        const { _id } = req.params;
        try {
            const checkDepartment = await Department.findOne({ _id });
            if (!checkDepartment) {
                return res.send({ status: false, msg: 'Department not exists with this.' })
            }

            checkDepartment.is_deleted = true;
            checkDepartment.save();

            return res.send({ status: true, msg: 'Department archived successfully.' })
        } catch (error) {
            console.log("--error", error)
            return res.send({ status: false, msg: "Something went wrong" })
        }
    }

    async list(req, res) {
        try {
            let data = await Department.find({ is_deleted: false }).sort({ _id: -1 });

            global.current_tab = "Department List";
            res.render('views/department/list', { data, baseurl: process.env.S3_BASE_URL });
        } catch (error) {
            global.current_tab = "Dashboard";
            res.render('views/dashboard');
        }
    }
}

module.exports = new DepartmentController();