const express = require("express"),
    router = express.Router(),
    CityController = require("./CityController"),
    AuthMainAdmin = require('../../../_helpers/authMainAdmin');


router.get("/", AuthMainAdmin, CityController.list);

router.all("/add",AuthMainAdmin, CityController.add);

router.post("/get_city_state", AuthMainAdmin, CityController.getCityState);

router.post("/edit",AuthMainAdmin, CityController.edit);

router.delete("/:_id",AuthMainAdmin, CityController.delete);

router.post("/update",AuthMainAdmin, CityController.update);

router.post("/update-status",AuthMainAdmin, CityController.updateStatus);

module.exports = router; 