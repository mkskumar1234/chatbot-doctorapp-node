let City = require("../../../models/City");
let State = require("../../../models/State");

class CityController {
    async add(req, res) {
        if (req.method == 'POST') {
            const { title, state_id } = req.body;
            try {
                const alreadyExist = await City.findOne({ title }).lean();
                if (alreadyExist) {
                    return res.status(200).json({ status: false, msg: 'City already exists.' })
                }
                let cities = new City();
                cities.state_id = state_id;
                cities.title = title;

                cities.save();

                return res.status(200).json({ status: true, msg: 'City added succesfully.' })
            } catch (error) {
                return res.status(400).json({ status: false, msg: 'Something went wrong.' })
            }
        } else {
            let states = await State.find({ is_deleted: false }).sort({ title: -1 });

            global.current_tab = "City Add";
            res.render('views/cities/add',{states});
        }

    }

    async view(req, res) {
        const { _id } = req.params;
        try {
            const result = await City.findOne({ _id });

            global.current_tab = "City Add";
            res.render('views/cities/view', {data: result});
        } catch (error) {
            console.log("--error", error)
            return res.status(400).json({ msg: "Something went wrong" })
        }
        }

    async update(req, res) {
        const { id, title } = req.body;
        try {
            const checkCity = await City.findOne({ _id: id });
            if (!checkCity) {
                return res.send({ status: false, msg: 'City not exists with this.' })
            }

            const checkDuplicate = await City.findOne({ _id: { $ne: id }, title });
            if (checkDuplicate) {
                return res.send({ status: false, msg: 'City already exists.' })
            }

            checkCity.title = title;

            checkCity.save();

            return res.send({ status: true, msg: 'City updated succesfully.' })
        } catch (error) {
            console.log("--error", error)
            return res.send({ status: false, msg: "Something went wrong" })
        }
    }

    async updateStatus(req, res) {
        const { _id, status } = req.body;
        try {
            const checkCity = await City.findOne({ _id });
            if (!checkCity) {
                return res.send({ status: false, msg: 'City not exists with this.' })
            }

            checkCity.status = status;
            checkCity.save();

            return res.send({ status: true, msg: 'Status updated succesfully.' })
        } catch (error) {
            console.log("--error", error)
            return res.send({ status: false, msg: "Something went wrong" })
        }
    }

    async edit(req, res) {
        const { id } = req.body;
        try {
            const checkCity = await City.findOne({ _id: id });
            if (!checkCity) {
                res.send({ status: false, msg: 'City not exists with this.' })
            }
            res.send({ status: true, data: checkCity });
        } catch (error) {
            console.log("--error", error)
            res.send({ status: false, msg: 'Something went wrong' })
        }
    }

    async delete(req, res) {
        console.log("00req.params", req.params)
        const { _id } = req.params;
        try {
            const checkCity = await City.findOne({ _id });
            if (!checkCity) {
                return res.send({ status: false, msg: 'City not exists with this.' })
            }

            checkCity.is_deleted = true;
            checkCity.save();

            return res.send({ status: true, msg: 'City archived successfully.' })
        } catch (error) {
            console.log("--error", error)
            return res.send({ status: false, msg: "Something went wrong" })
        }
    }

    async list(req, res) {
        try {
            let data = await City.find({ is_deleted: false }).populate("state_id",'title').sort({ _id: -1 });
            // console.log("-data",data)
            global.current_tab = "City List";
            res.render('views/cities/list', { data });
        } catch (error) {
            global.current_tab = "Dashboard";
            res.render('views/dashboard');
        }
    }
    async getCityState(req, res) {
        const { state_id } = req.body;
        try {
            let data = await City.find({ is_deleted: false, state_id }).sort({ _id: -1 });
            console.log("-data",data)
            return res.status(200).json({ data });
        } catch (error) {
            global.current_tab = "Dashboard";
            res.render('views/dashboard');
        }
    }
}

module.exports = new CityController();