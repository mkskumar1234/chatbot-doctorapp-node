let rules = {};

rules.register = {
    first_name:'required',
    last_name:'required',
    email: 'required|email',
    password : 'required|confirmed',
    password_confirmation:'required',
}

rules.login = {
    email: 'required|email',
    password : 'required'
}

rules.addCategory = {
    name: 'required'
}

module.exports.rules = rules;