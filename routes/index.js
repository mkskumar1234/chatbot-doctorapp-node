const express = require('express');
const router = express.Router();
const { ObjectId } = require('mongodb');

const Department = require("../models/Department");
const State = require("../models/State");
const City = require("../models/City");
const Doctor = require("../models/Doctor");


/* GET home page. */
router.get('/departments', async function (req, res) {
  try {
    let data = await Department.find({ is_deleted: false }).select('title').sort({ _id: -1 });

    return res.status(200).json({ data, msg: 'Department list' });
  } catch (error) {
    console.log("-error", error)
    return res.status(400).json({ msg: 'COMMON_ERROR' })
  }
});

router.get("/states", async (req, res) => {
  try {
    let data = await State.find({ is_deleted: false }).sort({ _id: -1 });

    return res.status(200).json({ data, msg: 'States list' });
  } catch (error) {
    return res.status(400).json({ msg: 'COMMON_ERROR' })
  }
});

router.get("/cities/:id", async (req, res) => {
  const { id } = req.params;
  try {
    //state_id: ObjectId(id)
    // let data = await City.find({ is_deleted: false, title: id }).sort({ _id: -1 });
    let data = await City.aggregate([
      {
        $lookup: {
          from: 'states',
          localField: 'state_id',
          foreignField: '_id',
          as: 'states',
        }
      },
      {
        $match: { 'states.title': id }

      },
      {
        $project: { 'title': 1 }
      },
      {
        $sort: { 'title': 1 }
      }
    ]);

    return res.status(200).json({ data, msg: 'City list' });
  } catch (error) {
    console.log("Err", error)
    return res.status(400).json({ msg: 'COMMON_ERROR' });
  }
});

router.post("/doctors", async (req, res) => {
  const { department_id, state_id, city_id } = req.body;
  try {
    // ,state_id: ObjectId(state_id) , city_id: ObjectId(city_id)  department_id: ObjectId(department_id) 
    // let data = await Doctor.find({ is_deleted: false }).select('name').sort({ _id: -1 });
    let data = await Doctor.aggregate([
      {
        $lookup: {
          from: 'departments',
          localField: 'department_id',
          foreignField: '_id',
          as: 'departments',
        }
      }, {
        $lookup: {
          from: 'states',
          localField: 'state_id',
          foreignField: '_id',
          as: 'states',
        }
      }, {
        $lookup: {
          from: 'cities',
          localField: 'city_id',
          foreignField: '_id',
          as: 'cities',
        }
      },
      {
        $match: { 'departments.title': department_id, 'states.title': state_id, 'cities.title': city_id }
      },
      {
        $project: { 'name': 1 }
      },
      {
        $sort: { 'name': 1 }
      }
    ]);

    return res.status(200).json({ data, msg: 'Doctors list' });
  } catch (error) {
    console.log("-error", error)
    return res.status(400).json({ msg: 'COMMON_ERROR' });
  }
});

router.post("/enquiry", async (req, res) => {
  const { location, department, doctor, is_recuring, meeting_slot, isRegister, registerNo, mobileNo, fullname, dob, email } = req.body;
  try {
    let data = await Doctor.find({ is_deleted: false, state_id: ObjectId(id) }).sort({ _id: -1 });

    return res.status(200).json({ data, msg: 'Doctors list' });
  } catch (error) {
    return res.status(400).json({ msg: 'COMMON_ERROR' });
  }
});

module.exports = router;
