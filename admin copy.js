var createError = require('http-errors');
var express = require('express'); 
var path = require('path');
const dotenv = require('dotenv');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
global.FUNC = require(process.cwd() + '/_helpers/func.js');


let indexRouter = require('./admin/routes/auth');
let departmentRouter = require('./admin/routes/department');
let stateRouter = require('./admin/routes/states');
let usersRouter = require('./admin/routes/users');
let doctorsRouter = require('./admin/routes/doctors');
let emailRouter = require("./admin/routes/email_templates")
let pageRouter = require("./admin/routes/pages")

var app = express();
dotenv.config();

global.site_title = process.env.SITE_TITLE;

// connection to mongodb
mongoose.connect(process.env.MONGO_URI, {
  /* other options */
  useNewUrlParser: true,
  useUnifiedTopology: true
});
mongoose.Promise = global.Promise;
mongoose.set('debug', true);
mongoose.set('useFindAndModify', false);

app.set("secretKey", process.env.SESSION_SECRET);
global.S3_BASE_URL = process.env.S3_BASE_URL;

// view engine setup
app.set('views', path.join(__dirname, 'admin'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb', extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/departments', departmentRouter);
app.use('/states', stateRouter);

app.use('/users', usersRouter);
app.use('/doctors', doctorsRouter);
app.use('/email-templates', emailRouter);
app.use('/pages', pageRouter);

app.get('/404',function (req, res) {
  var token = req.cookies.auth;
  if (token) {
    jwt.verify(token, process.env.SESSION_SECRET, (err, decoded) => {
      if (err) {
        next(err);
      } else {
        req.decoded = decoded;
        res.render('views/404', { role: req.decoded.role });
      }
    });
  }else{
    res.render('views/404', { role: '' });
  }
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  global.site_title = process.env.SITE_TITLE;
  global.site_url = process.env.ADMIN_URL;
  global.current_tab = "Error";

  // render the error page
  res.status(err.status || 500);
  res.render('views/error');
});

module.exports = app;