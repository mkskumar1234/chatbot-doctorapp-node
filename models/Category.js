const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;

const CategorySchema = new Schema({
	title: {
		type: String,
		trim: true,
		required: true,
	},
	title_ar: {
		type: String,
		trim: true,
		required: true,
	},
	image: {
		type: String,
		// required: true,
	},
	status: {
		type: Boolean,
		default: 1
	},
	is_deleted: {
		type: Boolean,
		default: false
	},
	__v: { type: Number, select: false }
}, {
		timestamps: true
	});

module.exports = mongoose.model('Category', CategorySchema);

