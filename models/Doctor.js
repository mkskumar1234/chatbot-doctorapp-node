const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;

const DoctorSchema = new Schema({
	name: {
		type: String,
		trim: true,		
		required: true,
	},
	email: {
        type: String,
        trim: true,
        required: true,
		lowercase: true,
		unique: true,
    },
    mobile: {
        type: String,
        trim: true,
        default: null
	},
	department_id:{
		type: Schema.Types.ObjectId,
        ref: 'Department',
        required: true
	},
	state_id:{
		type: Schema.Types.ObjectId,
        ref: 'State',
        required: true
	},
	city_id:{
		type: Schema.Types.ObjectId,
        ref: 'City',
        required: true
	},
	status:{ 
		type :Boolean,
		default : true
	},
	is_deleted: {
		type: Boolean,
		default: false
	},
	__v: { type: Number, select: false}
},{
	timestamps : true
});


module.exports = mongoose.model('Doctor', DoctorSchema);

