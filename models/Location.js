const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;

const LocationSchema = new Schema({
	name: {
		type: String,
		trim: true,		
		required: true,
		unique: true,
	},
	status:{
		type :Boolean,
		default : 1
	},
	isDeleted: {
		type: Boolean,
		default: false
	},
	__v: { type: Number, select: false}
},{
	timestamps : true
});


module.exports = mongoose.model('Location', LocationSchema);

