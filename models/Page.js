const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;

const PageSchema = new Schema({
	title: {
		type: String,
		trim: true,		
		required: true,
		unique: true,
	},
	slug: {
		type: String,
		trim: true,		
		required: true,
	},
	content: {
		type: String,
		trim: true,		
		required: true,
	},
	status:{
		type :Boolean,
		default : 1
	},
	isDeleted: {
		type: Boolean,
		default: false
	},
	__v: { type: Number, select: false}
},{
	timestamps : true
});


module.exports = mongoose.model('Page', PageSchema);

