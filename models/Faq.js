const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;

const FaqSchema = new Schema({
	question: {
		type: String,
		trim: true,		
		required: true,
		unique: true,
	},
	answer: {
		type: String,
		trim: true,		
		required: true,
	},
	status:{
		type :Boolean,
		default : 1
	},
	isDeleted: {
		type: Boolean,
		default: false
	},
	__v: { type: Number, select: false}
},{
	timestamps : true
});


module.exports = mongoose.model('Faq', FaqSchema);

