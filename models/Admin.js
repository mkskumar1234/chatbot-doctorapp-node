const mongoose = require("mongoose"),
    Schema = mongoose.Schema;
    bcrypt = require("bcrypt"),
    saltRound = 10;
    
const AdminSchema = new Schema({
    username: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true
    },
    forgot_pass_ver_code: {
        type: String,
        default: ''
    },
    verification_code: {
        type: String,
        default: ''
    },
    verification_expire: {
        type: String,
        default: ''
    },
}, {
    timestamps: true
});


AdminSchema.pre("save", async function(next){
    if(!this.isModified('password')) return next();

    try {
        this.password = await bcrypt.hash(this.password, saltRound);
        next();
    } catch (error) {
        next(error)
    }
})

module.exports = mongoose.model('Admin', AdminSchema);