const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;

const EmailTemplateSchema = new Schema({
	subject: {
		type: String,
		trim: true,		
		required: true
	},
	template_key: {
		type: String,
		trim: true,	
		unique: true,	
		required: true
	},
	content: {
		type: String,
		trim: true,		
		default: '',
	},
	status: {
		type: Boolean,
		default: 1
	},
	is_deleted: {
		type: String,
		default: 0
	},
	__v: { type: Number, select: false}
});


module.exports = mongoose.model('EmailTemplate', EmailTemplateSchema); 

