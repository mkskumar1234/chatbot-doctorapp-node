const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;

const CitySchema = new Schema({
	state_id: {
		type: Schema.Types.ObjectId,
        ref: "State",
		required: true
	},
	title: {
		type: String,
		trim: true,		
		required: true,
		unique: true,
	},
	status:{
		type :Boolean,
		default : 1
	},
	is_deleted: {
		type: Boolean,
		default: false
	},
	__v: { type: Number, select: false}
},{
	timestamps : true
});


module.exports = mongoose.model('City', CitySchema);

