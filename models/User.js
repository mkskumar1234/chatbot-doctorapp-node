const mongoose = require("mongoose"),
    Schema = mongoose.Schema;
    bcrypt = require("bcrypt"),
    saltRound = 10;

const UserSchema = new Schema({
    facebook_id: {
        type: String,
        trim: true
    },
    instagram_id: {
        type: String,
        trim: true
    },
    user_type: {
        type: String,
        enum: ['U','T'],
        default: 'U'
    },
    fullname: {
        type: String,
        trim: true,
        required: true
    },
    email: {
        type: String,
        trim: true,
        required: true,
        lowercase: true
    },
    username: {
        type: String,
        trim: true
    },
    postel_code: {
        type: String,
        trim: true,
        default: null
    },
    mobile: {
        type: String,
        trim: true,
        default: null
    },
    otp: {
        type: String,
        trim: true,
        default: null
    },
    country_code: {
        type: String,
        trim: true
    },
    image: {
        type: String,
        trim: true
    },
    dob: {
        type: Date,
        trim: true
    },
    password: {
        type: String,
        required: true
    },
    referal_code: {
        type: String,
        default: null
    },
    gender: {
        type: Boolean,
        default: true //true : male, false : female
    },
    status: {
        type: Boolean,
        default: true
    },
    device_token: {
        type: String,
        default: null,
    },
    device_type: {
        type: String,
        enum: ['A','I'],
        default: 'A'
    },
    is_deleted: {
        type: Boolean,
        default: false //false : not deleted, true : Deleted
    },
    __v:{
        type: Number,
        select : false
    }
},{
    timestamps: true
})

UserSchema.index({email:1});

UserSchema.pre("save", async function(next){
    if(!this.isModified('password')) return next();

    try {
        this.password = await bcrypt.hash(this.password, saltRound);
        next();
    } catch (error) {
        next(error)
    }
})

module.exports = mongoose.model("User", UserSchema);