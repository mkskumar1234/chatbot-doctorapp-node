const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;

const SettingSchema = new Schema({
	email: {
		type: String,
		trim: true,		
		required: false,
		default: ''
	},
	contact_details: {
		type: String,
		trim: true,		
		required: false,
		default: ''
	},
	address: {
		type: String,
		trim: true,		
		required: true,
	},
	force_update:{
		type: Boolean,
		trim:true,
		required:true,
		default:0
	},
	version:{
		type: String,
		trim:true,
		required:true
	},
	android_link: {
		type: String,
		trim: true,
		required: false,
		default: ''
	},
	facebook_id:{
		type: String,
		trim:true,
		required:false,
		default: ''
	},
	instagram_id:{
		type: String,
		trim:true,
		required:false,
		default: ''
	},
	twitter_id:{
		type: String,
		trim:true,
		required:false,
		default: ''
	},
	maintenance:{
		type: Boolean,
		required: true,
		default: false
	},
	maintenance_title: {
		type: String,
		required: true,
		default: 'Maintenance is Going On..'
	},
	maintenance_msg:{
		type: String,
		required: true,
		default: 'App is Under Maintenance, Please Try after Some Time.'
	}
},{
	timestamps : true
});


module.exports = mongoose.model('Setting', SettingSchema);

