var jwt = require('jsonwebtoken');
var Admin = require('../models/Admin');

module.exports = (req, res, next) => {
    var token = req.cookies.auth;
    if (token) {
        jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
            if (err) {
                res.redirect('/login');
            } else {
                Admin.findOne({
                    _id: decoded.sub
                }, function (err, user) {
                    if (err) {
                        res.cookie('auth', '');
                        res.redirect('/login');
                    }
                    else{
                        if (user != '' && user != undefined) {
                            req.decoded = decoded;
                            next(); 
                        }
                        else{
                            res.cookie('auth', '');
                            res.redirect('/login');
                        }
                    }
                })
                
            }
        });
    } else {
        res.redirect('/login');
    }
}  