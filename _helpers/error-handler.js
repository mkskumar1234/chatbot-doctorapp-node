module.exports = errorHandler;

function errorHandler(err, req, res, next) {
    //console.log(err);
    if (typeof (err) === 'string') {
        // custom application error
        return res.status(200).json({ msg: err });
    }

    if (err.name === 'ValidationError') {
        // mongoose validation error
        return res.status(203).json({ ms: err.message });
    }

    if (err.name === 'UnauthorizedError') {
        // jwt authentication error
        return res.status(440).json({ msg: LANG.INVALID_TOKEN });
    }

    // default to 500 server error
    return res.status(500).json({ msg: err.message });
}