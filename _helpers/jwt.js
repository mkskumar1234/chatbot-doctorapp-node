const expressJwt = require("express-jwt");
const User = require("../models/User");

function jwt(){
    const { secret } = { secret: process.env.SECRET_KEY };
    return expressJwt({secret, isRevoked}).unless({
        path:[
            '/auth/register',
            '/auth/login',
            '/test',
            "/common/departments",
            "/common/states",
            "/common/cities/*",
        ]
    })
}

async function isRevoked(req, payload, done){
    console.log("--req", req.headers);
    const user = await User.findOne({_id: payload.sub});
    console.log("--user", user);
    // revoke token if no longer exists
    if(!user){
        return done(null, true);
    }

    req.body.userId = user._id;

    done();
}

module.exports = jwt;