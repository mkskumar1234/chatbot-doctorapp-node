const Setting = require("../models/Setting");
const User = require("../models/User");
const bcrypt = require("bcrypt");
const nodemailer = require('nodemailer');
var path = require('path');
const dotenv = require('dotenv');
dotenv.config();
var FCM = require('fcm-node');
var fcm_server_keys = process.env.FCM_SERVER_KEY;
var fcmObj = new FCM(fcm_server_keys);

module.exports = {
    randonString: function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 15; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    },
    sendNotification: function(objData){
        var message = { 
            registration_ids: objData.deviceToken,
            //to : "dcaV-jg3qKM:APA91bGP1RADmgJNHkqKs8HHghg7H7p4_qYLGFoUFvCIg3j0JsAw9gJFMkgG3cEOGRwaAzrts5EOz3708pg7xdEbHnnQ5iNabgMpoapookoRo9FmuKfY4cTX6b42mVKRIdcdBPHeC5hI",
            // registration_ids : 'devicetoken' // use "to","registration_ids" for send single device notification 
            //collapse_key: 'your_collapse_key',
            priority: "high",
            notification: {
                title: objData.title, 
                body: objData.message
            },
            data : objData.data
        };
        fcmObj.send(message, function(err, response){
            console.log("=====response---------",response);
            return true;
        });
    },
    validateApi: function (rulesObject) {
        return function (req, res, next) {
            // Validating Input 
            var validation = new Validator(req.body, rulesObject, {
                "confirmed.password":"Confirm password not match to password."
            });
            if (validation.fails()) {
                //Validating fails 
                var errorObj = validation.errors.all();
                return res.status(203).send({
                    status: 203,
                    message: errorObj[Object.keys(errorObj)[0]][0],
                    metaData : []
                });
            } else {
                return next();
            }
        }
    },
    checkHeaders: function(callback){
        Setting.find().lean().limit(1).exec(function(err,settingInfo){
            if (err) {
                console.log("setting not found");
            } else {
                appSettings = settingInfo[0];
                console.log("waitappSettings");
                callback(appSettings);
            }
        })
    },
    comparePassword: function(){
        return function (req, res, next) {
            User.findOne({ _id : req.body.userId }).lean().exec(function(err,userInfo){
                if (err) {
                    res.status(200).json({
                        status: 400, 
                        message: LG.COMMON_ERROR, 
                        data: {},
                        meta : req.metaData
                    });
                } else {
                    if(!bcrypt.compareSync(req.body.old_password, userInfo.password)) {
                        return res.status(200).send({
                            status: 400,
                            message: LG.PASSWORD_MISMATCH,
                            data: {},
						    meta : req.metaData
                        });
                    }else{
                        return next();
                    }
                }
            })
        }
    },
    sendMail: function(mailData, callback) {
        
        // Prepare nodemailer transport object
        var transport = nodemailer.createTransport({
            from: process.env.SMTP_FROM,
            host: process.env.SMTP_HOST, // hostname
            service: process.env.SMTP_SERVICE,
            auth: {
                user: process.env.SMTP_AUTH_USER,
                pass: process.env.SMTP_AUTH_PASS
            }
        });

        // Send a single email
        transport.sendMail(mailData, function(err, responseStatus) {
            if (err) {
                return callback(err);
            } else {
                return callback(null);
            }
        });
    },
    checkVerificaton: function(reqData, callback){
        if(reqData){
            User.findOne({verification_code:reqData},function(err,userInfo){
                if (err) {
                    return callback(err);
                } else {
                    if(userInfo != null){
                        return callback(true);
                    }else{
                        return callback(false);
                    }
                }
            })
        }
    },
    checkResetPassword: function(reqData, callback){
        if(reqData){
            User.findOne({forgot_pass_ver_code:reqData},function(err,userInfo){
                if (err) {
                    return callback(err);
                } else {
                    if(userInfo != null) {
                        var CurrentDate = new Date();
                        CurrentDate = CurrentDate.getTime()/1000;
                        if(userInfo.verification_expire > CurrentDate){
                            return callback(true);
                        }else{
                            return callback(false);
                        }
                        return callback(true);
                    }else{
                        return callback(false);
                    }
                }
            })
        }
    },
    validateResetPassword: function(formData, callback){
        if(formData.password.length < 8){
            return callback('min');
        }
        if(formData.password == formData.password_confirmation){
            return callback(false);
        }else{
            return callback('mismatch');
        }
    }
}

